package persistence.mapping;



import org.mongolink.domain.mapper.AggregateMap;
import domain.car.Car;

@SuppressWarnings("UnusedDeclaration")
public class CarMapping extends AggregateMap<Car> {

    @Override
    public void map() {
        id().onProperty(element().getId()).auto();
        property().onField("name");
        property().onField("license");
        property().onProperty(element().getCreationDate());
    }
}
