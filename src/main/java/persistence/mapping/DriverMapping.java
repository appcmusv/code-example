package persistence.mapping;


import domain.driver.Driver;
import org.mongolink.domain.mapper.AggregateMap;

@SuppressWarnings("UnusedDeclaration")
public class DriverMapping extends AggregateMap<Driver> {

    @Override
    public void map() {
        id().onProperty(element().getId()).auto();
        property().onField("name");
        property().onField("age");
        property().onProperty(element().getCreationDate());
    }
}
