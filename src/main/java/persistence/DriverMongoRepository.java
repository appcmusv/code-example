package persistence;

/**
 * Created by clark on 11/1/2016.
 */

import domain.driver.Driver;
import domain.driver.DriverRepository;
import org.mongolink.MongoSession;

public class DriverMongoRepository extends MongoRepository<Driver> implements DriverRepository {
    public DriverMongoRepository(MongoSession mongoSession) {
        super(mongoSession);
    }

}