package persistence;

import domain.driver.DriverRepository;
import org.mongolink.MongoSession;
import domain.car.CarRepository;
import domain.Repositories;

public class MongoRepositories extends Repositories {

    public MongoRepositories(MongoSession session) {
        this.session = session;
    }

    @Override
    protected CarRepository carsRepository() {
        return new CarMongoRepository(session);
    }

    @Override
    protected DriverRepository driversRepository() { return new DriverMongoRepository(session); }

    private MongoSession session;


}
