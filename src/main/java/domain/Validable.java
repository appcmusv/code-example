package domain;

/**
 * Created by clark on 11/8/2016.
 */
public interface Validable {
    boolean validate() throws Exception;
}
