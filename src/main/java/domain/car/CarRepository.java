package domain.car;

import domain.Repository;

/**
 * Created by clark on 11/1/2016.
 */
public interface CarRepository extends Repository<Car> {
}
