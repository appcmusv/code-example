package domain.car;

import domain.Validable;
import org.joda.time.DateTime;

/**
 * Created by clark on 11/1/2016.
 */

public class Car implements Validable, Cloneable {

    @SuppressWarnings("UnusedDeclaration")
    protected Car() {
        // for mongolink
    }

    public Car(String name, String license) {
        this.name = name;
        this.license = license;
    }

    public Object getId() { return id; }

    public String getName() {
        return name;
    }

    public String getLicense() {
        return license;
    }

    public long getCreationDate() { return creationDate; }

    public void setLicense(String license) { this.license = license; }

    public void setName(String name) {
        this.name = name;
    }


    private Object id;
    private String name;
    private String license;
    private long creationDate = new DateTime().toDate().getTime();


    public boolean validate() throws Exception{
        //Could set up any additional validation rule
        if (this.license.length() < 8){
            throw new Exception("The License Plate Should Have At Least 8 Characters");
        }
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
