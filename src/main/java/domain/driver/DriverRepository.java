package domain.driver;

import domain.Repository;

/**
 * Created by clark on 11/1/2016.
 */
public interface DriverRepository extends Repository<Driver> {
}
