package domain.driver;

import domain.Validable;
import org.joda.time.DateTime;

/**
 * Created by clark on 11/8/2016.
 */
public class Driver implements Validable, Cloneable {
    @SuppressWarnings("UnusedDeclaration")
    protected Driver() {
        // for mongolink
    }

    public Driver(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Object getId() { return id; }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public long getCreationDate() { return creationDate; }

    public void setAge(int age) { this.age = age; }

    public void setName(String name) {
        this.name = name;
    }


    private Object id;
    private String name;
    private int age;
    private long creationDate = new DateTime().toDate().getTime();


    public boolean validate() throws Exception{
        //Could set up any additional validation rule
        if (this.age < 18){
            throw new Exception("The Driver Should be at Least 18 Years Old");
        }
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
