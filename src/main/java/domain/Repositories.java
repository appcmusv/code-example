package domain;

import domain.car.CarRepository;
import domain.driver.DriverRepository;

/**
 * Created by clark on 11/1/2016.
 */
public abstract class Repositories {

    public static void initialise(Repositories instance) {
        Repositories.instance = instance;
    }

    public static CarRepository cars() {
        return instance.carsRepository();
    }

    public static DriverRepository drivers() {
        return instance.driversRepository();
    }

    protected abstract CarRepository carsRepository();
    protected abstract DriverRepository driversRepository();

    private static Repositories instance;
}
