package controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Repositories;
import domain.car.Car;
import mongo.MongoConfiguration;
import org.mongolink.MongoSession;
import persistence.MongoRepositories;
import spark.Request;
import spark.Response;
import spark.Route;
import utils.JsonHandler;
import java.util.List;

/**
 * Created by clark on 11/8/2016.
 */
public class CarController extends JsonHandler {
    public static Route GetAllCars = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        List<Car> car = Repositories.cars().all();

        session.stop();
        response.status(200);
        return dataToJson(car);
    };

    public static Route GetCar = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));


        Car car = Repositories.cars().get(request.params(":id"));

        session.stop();
        response.status(200);
        return dataToJson(car);
    };

    public static Route PostCar = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        try{
            ObjectMapper mapper = new ObjectMapper();
            Car car = mapper.readValue(request.body(), Car.class);

            try {
                car.validate();
            } catch (Exception e){
                response.status(400);
                return dataToJson(e.getMessage());
            }

            Repositories.cars().add(car);

            session.stop();
            response.status(201);
            return dataToJson(car);

        }catch (JsonParseException e){
            session.stop();
            response.status(400);
            return dataToJson(e.getMessage());
        }
    };

    public static Route PatchCar = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        Car car = Repositories.cars().get(request.params(":id"));
        Car validationCar = (Car) car.clone();

        try{
            ObjectMapper mapper = new ObjectMapper();
            Car updatedCar = mapper.readValue(request.body(), Car.class);

            if(updatedCar.getName() != null){
                if(!updatedCar.getName().isEmpty()){
                    validationCar.setName(updatedCar.getName());
                }
            }

            if(updatedCar.getLicense() != null){
                if(!updatedCar.getLicense().isEmpty()){
                    validationCar.setLicense(updatedCar.getLicense());
                }
            }

            try{
                validationCar.validate();
            }catch (Exception e){
                session.stop();
                response.status(400);
                return dataToJson(e.getMessage());
            }

            car.setName(validationCar.getName());
            car.setLicense(validationCar.getLicense());
            session.stop();
            response.status(200);
            return dataToJson("Car Updated");

        }catch (JsonParseException e){
            session.stop();
            response.status(400);
            return dataToJson(e.getMessage());
        }
    };

    public static Route DeleteCar = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        Car car = Repositories.cars().get(request.params(":id"));
        Repositories.cars().delete(car);

        session.stop();
        response.status(200);
        return dataToJson("Car Deleted");
    };
}
