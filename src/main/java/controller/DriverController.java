package controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.DBObject;
import domain.Repositories;
import domain.driver.Driver;
import mongo.MongoConfiguration;
import org.mongolink.MongoSession;
import org.mongolink.domain.criteria.Criteria;
import org.mongolink.domain.criteria.Restrictions;
import persistence.MongoRepositories;
import spark.Request;
import spark.Response;
import spark.Route;
import utils.JsonHandler;

import java.util.List;

/**
 * Created by clark on 11/8/2016.
 */
public class DriverController extends JsonHandler {
    public static Route GetAllDrivers = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        Criteria criteria = session.createCriteria(Driver.class);
        criteria.add(Restrictions.equals("name", "juan"));
        criteria.add(Restrictions.greaterThanOrEqualTo("age", 18));
        /*DBObject db = criteria.createQuery();*/
        List<Driver>  crit = criteria.list();

        List<Driver> drivers = Repositories.drivers().all();

        session.stop();
        response.status(200);
        return dataToJson(drivers);
    };

    public static Route GetDriver = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));


        Driver driver = Repositories.drivers().get(request.params(":id"));

        session.stop();
        response.status(200);
        return dataToJson(driver);
    };

    public static Route PostDriver = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        try{
            ObjectMapper mapper = new ObjectMapper();
            Driver driver = mapper.readValue(request.body(), Driver.class);

            try {
                driver.validate();
            } catch (Exception e){
                response.status(400);
                return dataToJson(e.getMessage());
            }

            Repositories.drivers().add(driver);

            session.stop();
            response.status(201);
            return dataToJson(driver);

        }catch (JsonParseException e){
            session.stop();
            response.status(400);
            return dataToJson(e.getMessage());
        }
    };

    public static Route PatchDriver = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        Driver driver = Repositories.drivers().get(request.params(":id"));
        Driver validationDriver = (Driver) driver.clone();

        try{
            ObjectMapper mapper = new ObjectMapper();
            Driver updatedDriver = mapper.readValue(request.body(), Driver.class);

            if(updatedDriver.getName() != null){
                if(!updatedDriver.getName().isEmpty()){
                    validationDriver.setName(updatedDriver.getName());
                }
            }

            if(updatedDriver.getAge() != 0){
                validationDriver.setAge(updatedDriver.getAge());
            }

            try{
                validationDriver.validate();
            }catch (Exception e){
                session.stop();
                response.status(400);
                return dataToJson(e.getMessage());
            }

            driver.setName(validationDriver.getName());
            driver.setAge(validationDriver.getAge());
            session.stop();
            response.status(200);
            return dataToJson("Driver Updated");

        }catch (JsonParseException e){
            session.stop();
            response.status(400);
            return dataToJson(e.getMessage());
        }
    };

    public static Route DeleteDriver = (Request request, Response response) -> {
        final MongoSession session = MongoConfiguration.createSession();

        session.start();
        Repositories.initialise(new MongoRepositories(session));

        Driver driver = Repositories.drivers().get(request.params(":id"));
        Repositories.drivers().delete(driver);

        session.stop();
        response.status(200);
        return dataToJson("Driver Deleted");
    };
}
