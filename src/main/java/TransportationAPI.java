import static spark.Spark.*;

import controller.CarController;
import controller.DriverController;


public class TransportationAPI {
    public static void main(String[] args) {
        /* Spark Configuration */
        port(4567);
        /* Enable Debug */
        exception(Exception.class, (e, req, res) -> e.printStackTrace());

        /* Default Response Type */
        before((request, response) -> {
            response.type("application/json");
        });

        /* Car Routes */
        get("/cars", CarController.GetAllCars);
        get("/cars/:id", CarController.GetCar);
        post("/cars", CarController.PostCar);
        patch("/cars/:id", CarController.PatchCar);
        delete("/cars/:id", CarController.DeleteCar);

        /* Driver Routes */
        get("/drivers", DriverController.GetAllDrivers);
        get("/drivers/:id", DriverController.GetDriver);
        post("/drivers", DriverController.PostDriver);
        patch("/drivers/:id", DriverController.PatchDriver);
        delete("/drivers/:id", DriverController.DeleteDriver);

    }
}