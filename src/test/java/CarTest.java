import domain.car.Car;
import spark.Spark;
import utils.JsonHandler;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by clark on 11/24/2016.
 */
public class CarTest {

    @Before
    public void setUp() throws Exception {
        TransportationAPI.main(null);
    }

    @After
    public void tearDown() throws Exception {
        Spark.stop();
    }

    @Test
    public void a_testAddDriver() {
        Car car = new Car("test","test");

        TestResponse response = TestResponse.request("POST", "/cars", JsonHandler.dataToJson(car));
        Map<String, String> json = response.json();
        assertEquals(201, response.status);
        assertEquals("test", json.get("name"));
        assertEquals("test", json.get("license"));
    }
}
